import { SetupContext } from "vue";
import { TooltipPlacement, TooltipProps } from "../tooltip.props";
import { RectDirection, RectSizeProperty } from "./types";

export function useAdjustPlacement(props: TooltipProps, context: SetupContext) {

    const revertDirectionMap = new Map<RectDirection, RectDirection>(
        [['top', 'bottom'], ['bottom', 'top'], ['left', 'right'], ['right', 'left']]
    );

    const directionBoundMap = new Map<RectDirection, RectSizeProperty>(
        [['top', 'height'], ['bottom', 'height'], ['left', 'width'], ['right', 'width']]
    );

    function revert(placement: string, direction: RectDirection): TooltipPlacement {
        const revertDirection: RectDirection = revertDirectionMap.get(direction) || direction;
        const revertedPlacement = placement.replace(direction, revertDirection) as TooltipPlacement;
        return revertedPlacement;
    }

    function adjustPlacement(
        placementAndAlignment: TooltipPlacement,
        referenceBoundingRect: DOMRect,
        arrowReferenceBoundingRect: DOMRect,
        tooltipBound: DOMRect,
        arrowBound: DOMRect): TooltipPlacement {
        let adjustedResult = placementAndAlignment;
        const placement = placementAndAlignment.split('-')[0] as RectDirection;
        const boundProperty = directionBoundMap.get(placement) as RectSizeProperty;
        const boundSize = tooltipBound[boundProperty] + arrowBound[boundProperty];
        const referenceBoundSize = Math.abs(arrowReferenceBoundingRect[placement] - referenceBoundingRect[placement]);

        if (referenceBoundSize < boundSize) {
            adjustedResult = revert(placementAndAlignment, placement);
        }
        return adjustedResult;
    }

    return { adjustPlacement };
}
