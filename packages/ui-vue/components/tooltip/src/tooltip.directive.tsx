import { App, createApp, onUnmounted, reactive } from "vue";
import { TooltipProps } from "./tooltip.props";
import Tooltip from "./tooltip.component";

function initInstance(props: TooltipProps, content?: string): App {
    const container = document.createElement('div');
    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            return () => <Tooltip {...props} onClick={app.unmount}></Tooltip>;
        }
    });
    document.body.appendChild(container);
    app.mount(container);
    return app;
}

function showTooltip(options: TooltipProps): App {
    const props: TooltipProps = reactive({
        ...options
    });
    return initInstance(props);
}

const tooltipDirective = {
    mounted: (element: HTMLElement, binding: Record<string, any>, vnode: any) => {
        let app: App | null;
        const tooltipProps = Object.assign({
            content: 'This is a tooltip',
            width: 100,
            customClass: '',
            placement: 'top',
            reference: element
        }, binding.value);
        element.addEventListener('mouseenter', ($event: MouseEvent) => {
            $event.stopPropagation();
            if (!app) {
                app = showTooltip(tooltipProps);
            }
        });
        element.addEventListener('mouseleave', ($event: MouseEvent) => {
            $event.stopPropagation();
            if (app) {
                app.unmount();
                app = null;
            }
        });
    },
    unMounted: (element: HTMLElement, binding: Record<string, any>, vnode: any) => { }
};
export default tooltipDirective;
