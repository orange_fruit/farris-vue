import { ExtractPropTypes, PropType } from 'vue';

type AvatarShap = 'square' | 'circle';

export const avatarProps = {
    /**
     * 头像宽度
     */
    avatarWidth: { type: Number, default: 100 },
    /**
     * 头像高度
     */
    avatarHeight: { type: Number, default: 100 },
    /**
     * 组件标识
     */
    cover: { type: String },
    /**
     * 只读
     */
    readonly: { type: Boolean, default: false },
    /**
     * 头像形状
     */
    shape: { type: String as PropType<AvatarShap>, default: 'circle' },
    /**
     * 头像最大尺寸, 单位MB
     */
    maxSize: { type: Number, default: 1 },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },
    /**
     * 头像标题
     */
    tile: { type: String, default: '' },
    /**
     * 支持的头像类型
     */
    type: { type: Array<string>, default: [] }
};

export type AvatarProps = ExtractPropTypes<typeof avatarProps>;
