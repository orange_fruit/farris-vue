import { ComputedRef } from 'vue';

export interface UseImage {
    acceptTypes: ComputedRef<string>;

    imageSource: ComputedRef<string>;

    imageTitle: ComputedRef<string>;

    onClickImage: () => void;
}

export interface ImageFile {
    size: number;
    name: string;
    type: string;
    lastModified?: string;
    lastModifiedDate?: Date;
    state?: string;
    base64?: string;
}
