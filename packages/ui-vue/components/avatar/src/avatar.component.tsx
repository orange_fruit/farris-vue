import { defineComponent, computed, ref, SetupContext } from 'vue';
import { avatarProps, AvatarProps } from './avatar.props';
import { useImage } from './composition/use-image';

import './avatar.scss';

export default defineComponent({
    name: 'FAvatar',
    props: avatarProps,
    emits: ['change', 'update:modelValue'],
    setup(props: AvatarProps, context: SetupContext) {
        const avatarClass = computed(() => ({
            'f-avatar': true,
            'f-avatar-readonly': props.readonly,
            'f-avatar-circle': props.shape === 'circle',
            'f-avatar-square': props.shape === 'square'
        }));

        const modelValue = ref(props.modelValue);

        const avatarStyle = computed(() => ({
            width: props.avatarWidth + 'px',
            height: props.avatarHeight + 'px'
        }));

        let showLoading = false;
        let imgSrc = '';

        const currentImgType = ['image/image', 'image/webp', 'image/png', 'image/svg', 'image/gif', 'image/jpg', 'image/jpeg', 'image/bmp'];

        function errorSrc() {
            return '';
        }

        function getfiledata() {}

        const file = ref(null);

        const { acceptTypes, imageSource, onClickImage } = useImage(props, context, file, modelValue);

        return () => {
            return (
                <div class={avatarClass.value} style={avatarStyle.value} onClick={onClickImage}>
                    {showLoading && (
                        <div class="f-avatar-upload-loading">
                            <div class="loading-inner">加载中</div>
                        </div>
                    )}
                    <img title={props.tile} class="f-avatar-image" src={imageSource.value} onError={errorSrc()} />
                    {!props.readonly && (
                        <div class="f-avatar-icon">
                            <span class="f-icon f-icon-camera"></span>
                        </div>
                    )}
                    <input
                        ref="file"
                        name="file-input"
                        type="file"
                        class="f-avatar-upload"
                        accept={acceptTypes.value}
                        onChange={getfiledata}
                        style="display: none;"
                    />
                </div>
            );
        };
    }
});
