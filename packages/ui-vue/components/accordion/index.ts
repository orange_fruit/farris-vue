import type { App } from 'vue';
import Accordion from './src/accordion.component';
import AccordionItem from './src/components/accordion-item.component';

export * from './src/accordion.props';
export * from './src/components/accordion-item.props';

export { Accordion, AccordionItem };

export default {
    install(app: App): void {
        app.component(Accordion.name, Accordion);
        app.component(AccordionItem.name, AccordionItem);
    }
};
