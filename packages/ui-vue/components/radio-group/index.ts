import type { App } from 'vue';
import RadioGroup from './src/radio-group.component';

export * from './src/radio-group.props';

export { RadioGroup };

export default {
    install(app: App): void {
        app.component(RadioGroup.name, RadioGroup);
    }
};
