import { ComputedRef, Ref } from 'vue';

export interface Radio {
    /**
     * 枚举值
     */
    value: ComputedRef<any>;
    /**
     * 枚举展示文本
     */
    name: ComputedRef<any>;
}

export interface ChangeRadio {

    enumData: ComputedRef<Array<Radio>>;

    /**
     * 获取枚举值
     */
    getValue(item: Radio): any;
    /**
     * 获取枚举文本
     */
    getText(item: Radio): any;

    /**
     * 切换单选按钮事件
     */
    onClickRadio: (item: Radio, $event: Event) => void;

}
