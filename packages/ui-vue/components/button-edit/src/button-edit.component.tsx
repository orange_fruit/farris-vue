import { defineComponent, computed, ref } from 'vue';
import type { SetupContext } from 'vue';
import { buttonEditProps, ButtonEditProps } from './button-edit.props';
import { useButton } from './composition/use-button';
import { useClear } from './composition/use-clear';
import { useTextBox } from './composition/use-text-box';

export default defineComponent({
    name: 'FButtonEdit',
    props: buttonEditProps,
    emits: [
        'updateExtendInfo',
        'clear',
        'change',
        'click',
        'clickButton',
        'blur',
        'focus',
        'mouseEnterIcon',
        'mouseLeaveIcon',
        'keyup',
        'keydown',
        'inputClick',
        'input',
        'update:modelValue',
    ],
    setup(props: ButtonEditProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const { buttonClass, onClickButton, onMouseEnterButton, onMouseLeaveButton } = useButton(props, context);
        const displayText = ref('');
        const {
            hasFocusedTextBox,
            isTextBoxReadonly,
            textBoxClass,
            textBoxPlaceholder,
            textBoxTitle,
            onBlurTextBox,
            onClickTextBox,
            onFocusTextBox,
            onInput,
            onKeyDownTextBox,
            onKeyUpTextBox,
            onMouseDownTextBox,
            onTextBoxValueChange,
        } = useTextBox(props, context, modelValue, displayText);

        const { enableClearButton, showClearButton, onClearValue, onMouseEnterTextBox, onMouseLeaveTextBox } = useClear(
            props,
            context,
            modelValue,
            hasFocusedTextBox,
            displayText
        );

        const inputGroupClass = computed(() => ({
            'input-group': true,
            'f-state-disable': props.disable,
            'f-state-editable': props.editable && !props.disable && !props.readonly,
            'f-state-readonly': props.readonly && !props.disable,
            'f-state-focus': hasFocusedTextBox,
        }));

        return () => {
            return (
                <div class="f-cmp-inputgroup" id={props.id}>
                    <div
                        class={[props.customClass, inputGroupClass.value]}
                        onMouseenter={onMouseEnterTextBox}
                        onMouseleave={onMouseLeaveTextBox}>
                        <input
                            name="input-group-value"
                            autocomplete={'' + props.autoComplete}
                            class={textBoxClass.value}
                            disabled={props.disable}
                            maxlength={props.maxLength}
                            minlength={props.minLength}
                            placeholder={textBoxPlaceholder.value}
                            readonly={isTextBoxReadonly.value}
                            tabindex={props.tabIndex}
                            title={textBoxTitle.value}
                            type={props.inputType}
                            value={modelValue.value}
                            onBlur={onBlurTextBox}
                            onChange={onTextBoxValueChange}
                            onClick={onClickTextBox}
                            onFocus={onFocusTextBox}
                            onInput={onInput}
                            onKeydown={onKeyDownTextBox}
                            onKeyup={onKeyUpTextBox}
                            onMousedown={onMouseDownTextBox}
                        />
                        <div class={buttonClass.value}>
                            {enableClearButton.value && (
                                <span class="input-group-text input-group-clear" v-show={showClearButton.value} onClick={onClearValue}>
                                    <i class="f-icon modal_close"></i>
                                </span>
                            )}
                            {props.buttonContent && (
                                <span
                                    class="input-group-text input-group-append-button"
                                    onClick={onClickButton}
                                    onMouseenter={onMouseEnterButton}
                                    onMouseleave={onMouseLeaveButton}
                                    v-html={props.buttonContent}></span>
                            )}
                        </div>
                    </div>
                </div>
            );
        };
    },
});
