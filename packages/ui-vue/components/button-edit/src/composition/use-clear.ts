import { computed, ComputedRef, Ref, ref, SetupContext, watch } from 'vue';
import { ButtonEditProps } from '../button-edit.props';
import { UseClear } from './types';
import { useTextBox } from './use-text-box';

export function useClear(
    props: ButtonEditProps,
    context: SetupContext,
    modelValue: Ref<string>,
    hasFocusedTextBox: ComputedRef<boolean>,
    displayText: Ref<string>
): UseClear {
    const showClearButton = ref(false);
    const enableClearButton = computed(() => props.enableClear && !props.readonly && !props.disable);
    const { changeTextBoxValue } = useTextBox(props, context, modelValue, displayText);

    function toggleClearIcon(isShow: boolean) {
        showClearButton.value = isShow;
    }

    watch(displayText, () => {
        if (hasFocusedTextBox.value) {
            toggleClearIcon(!!displayText.value);
        } else {
            toggleClearIcon(false);
        }
    });

    function onClearValue($event: Event) {
        const flag1 = !props.readonly && !props.disable && props.editable;
        const flag2 = !props.editable;
        $event.stopPropagation();
        if (flag1 || flag2) {
            changeTextBoxValue('', false);
            toggleClearIcon(!showClearButton.value);
            context.emit('clear');
        }
    }

    function onMouseEnterTextBox($event: Event) {
        if (!enableClearButton.value) {
            return;
        }
        if (!modelValue.value) {
            toggleClearIcon(false);
            return;
        }
        if ((!props.editable || !props.readonly) && !props.disable) {
            toggleClearIcon(true);
        }
    }

    function onMouseLeaveTextBox($event: Event) {
        if (!enableClearButton.value) {
            return;
        }
        toggleClearIcon(false);
    }

    return {
        enableClearButton,
        showClearButton,
        onClearValue,
        onMouseEnterTextBox,
        onMouseLeaveTextBox,
    };
}
