import { UseButton } from './types';
import { computed, SetupContext } from 'vue';
import { ButtonEditProps } from '../button-edit.props';

export function useButton(props: ButtonEditProps, context: SetupContext): UseButton {
    const buttonClass = computed(() => ({
        'input-group-append': true,
        'append-force-show': props.showButtonWhenDisabled && (props.readonly || props.disable),
    }));

    const canClickAppendButton = computed(() => props.showButtonWhenDisabled || ((!props.editable || !props.readonly) && !props.disable));

    function onClickButton($event: Event) {
        if (canClickAppendButton.value) {
            context.emit('clickButton', { origin: $event, value: props.modelValue });
        }
        $event.stopPropagation();
    }

    function onMouseEnterButton($event: MouseEvent) {
        context.emit('mouseEnterIcon', $event);
    }

    function onMouseLeaveButton($event: MouseEvent) {
        context.emit('mouseLeaveIcon', $event);
    }

    function onMouseOverButton() {
        context.emit('mouseOverButton');
    }

    return {
        buttonClass,
        onClickButton,
        onMouseEnterButton,
        onMouseLeaveButton,
        onMouseOverButton,
    };
}
