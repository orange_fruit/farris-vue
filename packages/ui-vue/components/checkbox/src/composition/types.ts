import { ComputedRef, Ref } from 'vue';

export interface Checkbox {
    /**
     * 枚举值
     */
    value: ComputedRef<any>;
    /**
     * 枚举展示文本
     */
    name: ComputedRef<any>;
}

export interface ChangeCheckbox {

    enumData: ComputedRef<Array<Checkbox>>;

    /**
     * 获取枚举值
     */
    getValue(item: Checkbox): any;
    /**
     * 获取枚举文本
     */
    getText(item: Checkbox): any;

    /**
     * 校验复选框是否为选中状态
     */
    checked(item: Checkbox): boolean;

    /**
     * 点击复选框事件
     */
    onClickCheckbox: (item: Checkbox, $event: Event) => void;

}
