import { defineComponent, computed, ref } from 'vue';
import type { SetupContext } from 'vue';
import { CheckboxGroupProps, checkboxGroupProps } from './checkbox-group.props';
import { useCheckboxGroup } from './composition/use-checkbox-group';

export default defineComponent({
    name: 'FCheckboxGroup',
    props: checkboxGroupProps,
    emits: [
        'changeValue',
        'update:modelValue',
    ],
    setup(props: CheckboxGroupProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const { enumData, onClickCheckbox, getValue, getText, checked, loadData } = useCheckboxGroup(props, context, modelValue);

        // 异步数据
        loadData();

        const horizontalClass = computed(() => ({
            'farris-checkradio-hor': props.horizontal
        }));

        return () => {
            return (
                <div class={['farris-input-wrap', horizontalClass.value]}>
                    {
                        enumData.value.map((item, index) => {
                            const id = 'checkbox_' + props.name + index;

                            return (
                                <div class="custom-control custom-checkbox" >
                                    <input
                                        type="checkbox"
                                        class="custom-control-input"
                                        name={props.name}
                                        id={id}
                                        value={getValue(item)}
                                        checked={checked(item)}
                                        disabled={props.disable}
                                        tabindex={props.tabIndex}
                                        onClick={(event: MouseEvent) => onClickCheckbox(item, event)}
                                    />
                                    <label class="custom-control-label" for={id} title={getText(item)}>
                                        {getText(item)}
                                    </label>
                                </div>
                            );
                        })
                    }

                </div>
            );
        };
    },
});
