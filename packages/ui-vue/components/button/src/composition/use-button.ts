import { UseButton } from './types';
import { ButtonProps } from '../button.props';
import { SetupContext } from 'vue';

export function useButton(props: ButtonProps, context: SetupContext): UseButton {

    function onClickButton($event: Event) {
        $event.stopPropagation();
        if (props.disabled) {
            context.emit('clickButton', $event);
        }
    }

    return {
        onClickButton
    };
}
