import { ExtractPropTypes, PropType } from 'vue';

type PlacementDirection =
    | 'top'
    | 'top-left'
    | 'top-right'
    | 'left'
    | 'left-top'
    | 'left-bottom'
    | 'bottom'
    | 'bottom-left'
    | 'bottom-right'
    | 'right'
    | 'right-top'
    | 'right-bottom';

export const buttonGroupProps = {
    /**
     * 组件标识
     */
    // id: String,
    /**
     * 横向纠正的参照
     */
    // rectifyReferenceH: { type: HTMLElement},
    // /**
    //  * 纵向纠正的参照
    //  */
    // rectifyReferenceV: { type: HTMLElement},
    // /**
    //  * 是否自动纠正位置
    //  */
    // autoRectify: { type: Boolean, default: false },
    // /**
    //  * 计算方向的真正placement
    //  */
    // realPlacement: { type: String, default: 'bottom-right' },
    // /**
    //  * 重新计算后的placement
    //  */
    // rectifyPlacement: { type: String, default: 'bottom-right' },
    /**
     * 按钮信息
     */
    data: { type: Array },
    /**
     * 显示的按钮数量  默认为2
     */
    count: { type: Number, default: 2 },
    // /**
    //  * 按钮大小
    //  */
    // size: { type: String, default: 'small' },
    // /**
    //  * 按钮样式
    //  */
    // type: { type: String, default: 'primary' },
    /**
     * 按钮展示位置
     */
    placement: { type: String as PropType<PlacementDirection>, default: 'bottom' }, /**
    // /**
    //  * 下拉面板显示
    //  */
    // showPanel: { type: Boolean, default: false },
    // /**
    //  * 下拉面板显示标志
    //  */
    // dpFlag: { type: Boolean, default: false }
};
export default buttonGroupProps;

export type ButtonGroupProps = ExtractPropTypes<typeof buttonGroupProps>;
