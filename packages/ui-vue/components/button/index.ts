import type { App } from 'vue';
import FButton from './src/button.component';
import FButtonGroup from './src/button-group.component';

export * from './src/button.props';
export * from './src/button-group.props';

export { FButton };

export default {
    install(app: App): void {
        app.component(FButton.name, FButton);
        app.component(FButtonGroup.name, FButtonGroup);
    },
};
