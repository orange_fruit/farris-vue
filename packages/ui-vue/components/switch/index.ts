import type { App } from 'vue';
import Switch from './src/switch.component';

export * from './src/switch.props';

export { Switch };

export default {
    install(app: App): void {
        app.component(Switch.name, Switch);
    }
};
