import { ExtractPropTypes, PropType } from 'vue';

export type SwitchType = 'small' | 'medium' | 'large';

export const switchProps = {
    square: { type: Boolean, default: false },
    size: { type: String as PropType<SwitchType>, default: 'medium' },
    switchOffColor: { type: String },
    switchColor: { type: String },
    defaultBackgroundColor: { type: String },
    defaultBorderColor: { type: String },
    checkedLabel: { type: String },
    uncheckedLabel: { type: String },
    checked: { type: Boolean, default: false },
    readonly: { type: Boolean },
    disable: { type: Boolean },
    editable: { type: Boolean, default: true },
    reverse: { type: Boolean },
    trueValue: { type: Object, default: true },
    falseValue: { type: Object, default: false },
    /**
     * 组件值
     */
    modelValue: { type: Boolean, default: false }
};
export type SwitchProps = ExtractPropTypes<typeof switchProps>;
