import { computed, inject, Ref, SetupContext } from "vue";
import { IUseOption } from "../types";
import { OptionProps } from "../combo-list.props";
import { COMBO_LIST_TOKEN } from "../const";

export function useOption(props: OptionProps, context: SetupContext): IUseOption {
  const comboList = inject(COMBO_LIST_TOKEN, null);
  const name = computed(() => {
    return props.name || props.value;
  });
  const onOptionClick = ($event: Event) => {
    if (!props.disabled) {
      comboList?.onValueChange({
        value: props.value,
        name: props.name,
        disabled: props.disabled
      });
    }
  };
  const isOptionSelected = computed(() => {
    return comboList?.modelValue === props.value;
  });
  const optionClass = computed(() => ({
    active: isOptionSelected.value,
    'list-group-item': true,
    'list-group-item-action': true
  }));
  return {
    name,
    optionClass,
    onOptionClick
  };
}