import { computed, reactive, ref, Ref, SetupContext } from "vue";
import { ComboListProps } from "../combo-list.props";
import { IOption, ModelValue } from "../types";

export function useComboList(props: ComboListProps, context: SetupContext, modelValue: Ref<ModelValue>, panelRef: Ref<HTMLDivElement | undefined>) {
  const isPanelVisible = ref(false);
  const displayText = ref<string | undefined>('');
  const panelContainerClass = computed(() => ({
    comboPanel: true,
    'f-area-hide': !isPanelVisible.value,
    'f-area-show': isPanelVisible.value
  }));
  //#region events
  /**
   * 清空事件
   * @param $event event
   */
  function onClear($event: Event) {
    //modelValue.value = '';
    context.emit('clear');
  }
  /**
   * 下拉按钮点击
   * @param $event event
   */
  function onButtonClick($event: Event) {
    const clickOutsideEventHandler = (event: any) => {
      if (panelRef.value == event.target || panelRef?.value?.contains(event.target)) {
        event.stopPropagation();
      } else {
        isPanelVisible.value = false;
      }
    };
    isPanelVisible.value = !isPanelVisible.value;
    if (isPanelVisible.value === true) {
      // 即将展示下拉面板
      document.addEventListener("click", clickOutsideEventHandler);
    } else {
      document.removeEventListener('click', clickOutsideEventHandler);
    }
  }
  /**
   * 
   * @param item 值变化事件处理器
   */
  function onValueChange(item: IOption) {
    modelValue.value = item.value;
    if (props.multiSelect) {

    } else {
      displayText.value = item.name;
      context.emit('update:modelValue', item.value);
    }
    isPanelVisible.value = false;
  }
  //#endregion
  return {
    panelContainerClass,
    isPanelVisible,
    displayText,
    onClear,
    onButtonClick,
    onValueChange
  };
}