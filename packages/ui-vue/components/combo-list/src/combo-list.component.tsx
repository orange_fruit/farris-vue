import { defineComponent, provide, reactive, ref, SetupContext, Teleport } from 'vue';
import { comboListProps, ComboListProps } from './combo-list.props';
import { ButtonEdit } from '../../button-edit';
import { ViewType } from './types';
import { COMBO_LIST_TOKEN, groupIcon } from './const';
import { useState } from './composition/use-state';
import { useComboList } from './composition/use-combo-list';
import FOptions from './components/options';


export default defineComponent({
  name: 'FComboList',
  props: comboListProps,
  emits: [
    'clear',
    'update:modelValue'
  ],
  inheritAttrs: false,
  setup(props: ComboListProps, context: SetupContext) {
    const modelValue = ref(props.modelValue);
    const panelRef = ref<HTMLDivElement | undefined>();

    // is panel visible
    const { isPanelVisible, panelContainerClass, displayText, onClear, onButtonClick, onValueChange } = useComboList(props, context, modelValue, panelRef);
    /**
     * provider
     */
    provide(
      COMBO_LIST_TOKEN,
      reactive({
        onValueChange,
        modelValue
      })
    );
    return () => {
      return (
        <>
          {/** main component */}
          <ButtonEdit
            class={{ active: isPanelVisible }}
            disable={props.disabled}
            readonly={props.readonly}
            forcePlaceholder={props.forcePlaceholder}
            editable={props.editable}
            buttonContent={groupIcon}
            placeholder={props.placeholder}
            enableClear={props.enableClear}
            maxLength={props.maxLength}
            style="display:block"
            v-model={displayText.value}
            onClear={onClear}
            onClickButton={onButtonClick}
          />
          {/** tag area */}
          {props.viewType === ViewType.Tag && (
            <div class="f-cmp-inputgroup">
              <div class="input-group">
                <div class="form-control f-cmp-inputgroup--multi-wrapper multi-more">
                  <div class="multi--content">
                    <span class="multi--item">
                      <i class="f-icon multi--close"></i>
                    </span>
                  </div>
                  <div class="multi--more">
                    <i class="f-icon multi--more-icon"></i><span class="multi--more-text"></span>
                  </div>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text input-group-clear ng-star-inserted" style="display: none;">
                    <i class="f-icon modal_close"></i>
                  </span>
                  <span class="input-group-text ng-star-inserted">
                    <span class="f-icon f-icon-arrow-60-down"></span>
                  </span>
                </div>
              </div>
            </div>
          )}
          {/** panel area */}
          {isPanelVisible.value && (
            <div class={panelContainerClass.value} ref={panelRef} style="z-index: 99999;position:relative;width:100%;">
              <div style="position: absolute;top:0;left:0;width:100%" class="f-datalist">
                <div className="card">
                  <FOptions options={props.data}></FOptions>
                </div>
              </div>
            </div>
          )}
        </>
      );
    };
  }
});