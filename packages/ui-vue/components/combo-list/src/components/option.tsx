import { defineComponent, SetupContext, withModifiers } from 'vue';
import { optionProps, OptionProps } from '../combo-list.props';
import { useOption } from '../composition/use-option';

export default defineComponent({
  name: 'FOption',
  props: optionProps,
  emits: [],
  inheritAttrs: false,
  setup(props: OptionProps, context: SetupContext) {
    const { name, optionClass, onOptionClick } = useOption(props, context);

    return () => {
      return (
        <li style="cursor: default;" class={optionClass.value} onClick={withModifiers(onOptionClick, ['prevent', 'stop'])}>
          <span class="f-datalist-item-text">
            {context.slots?.default ? context.slots.default() : name.value}
          </span>
        </li>
      );
    };
  }
});