import { ExtractPropTypes, PropType } from "vue";
import { Options, ViewType } from './types';

/**
 * 下拉列表属性
 */
export const comboListProps = {
  /**
   * 组件标识
   */
  id: { type: String },
  /**
   * 可选，是否可编辑
   * 默认`false`
   */
  editable: { default: false, type: Boolean },
  /**
   * 可选，是否禁用
   * 默认为`false`
   */
  disabled: { default: false, type: Boolean },
  /**
   * 可选，是否只读
   * 默认为`false`
   */
  readonly: { default: false, type: Boolean },
  /**
   * 最大输入长度
   */
  maxLength: { type: Number },
  /**
   * 占位符
   */
  placeholder: { type: String },
  /**
   * 可选，强制显示占位符
   * 默认`false`
   */
  forcePlaceholder: { default: false, type: Boolean },
  /**
   * 可选，是否启用清空
   * 默认启用
   */
  enableClear: { default: true, type: Boolean },
  /**
   * 可选，鼠标悬停时是否显示控件值
   * 默认显示
   */
  enableTitle: { default: true, type: Boolean },
  /**
   * 可选，下拉列表值展示方式
   * 支持text | tag，即文本或标签，默认为`ViewType.Text`，即文本方式`text`
   */
  viewType: { default: ViewType.Text, type: String as PropType<ViewType> },
  /**
   * 可选，字段映射
   */
  mapFields: { type: Object },
  /**
   * 下拉数据源
   */
  data: { type: Array as PropType<Options> },
  /**
   * 可选，数据源id字段
   * 默认为`id`
   */
  idField: { default: 'id', type: String },
  /**
   * 可选，数据源值字段
   * 默认为`id`
   */
  valueField: { default: 'id', type: String },
  /**
   * 可选，数据源显示字段
   * 默认为`label`
   */
  textField: { default: 'label', type: String },
  /**
   * 可选，是否支持多选
   * 默认`false`
   */
  multiSelect: { default: false, type: Boolean },
  /**
   * 数据源地址
   */
  uri: { type: String },
  /**
   * 可选，最大高度
   * 默认`200`
   */
  maxHeight: { default: 350, type: Number },
  /**
   * 可选，是否支持远端过滤
   * 默认`false`
   */
  remoteSearch: { default: false, type: Boolean },
  /**
   * 可选，清空值时隐藏面板
   * 默认`true`
   */
  hidePanelOnClear: { default: true, type: Boolean },
  /**
   * 可选，分隔符
   * 默认`,`
   */
  separator: { default: ',', type: String },
  /**
   * 可选，展示文本
   * 默认为空字符串
   */
  displayText: { type: String, default: '' },
  /**
   * 绑定值
   */
  modelValue: { type: String }

};
export type ComboListProps = ExtractPropTypes<typeof comboListProps>;

/**
 * option 属性
 */
export const optionProps = {
  /**
   * 必须，值
   */
  value: { required: true, type: [String, Number] as PropType<string | number> },
  /**
   * 必须，名称
   */
  name: { required: true, type: String },
  /**
   * 可选，是否禁用
   * 默认`false`
   */
  disabled: { default: false, type: Boolean }
};
export type OptionProps = ExtractPropTypes<typeof optionProps>;
/**
 * options 属性
 */
export const optionsProps = {
  options: {
    type: Array as PropType<Options>
  }
};
export type OptionsProps = ExtractPropTypes<typeof optionsProps>;