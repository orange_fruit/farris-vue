//import EventEmitter from 'events';
import { ComputedRef, Ref, UnwrapRef } from 'vue';
export type ModelValue = number | string | undefined | Array<number | string | undefined>;
/**
 * 数据展现方式
 */
export enum ViewType {
  Text = 'text',
  Tag = 'tag'
};
/**
 * IUseState type
 * @description 受vue类型系统限制，仅在类型定义中约定state只读，实际返回对象可写
 */
export type IUseState<T> = [Readonly<Ref<UnwrapRef<T>>>, (state: UnwrapRef<T>) => void];

export interface IUseEventHandler {
  /**
   * 清除事件
   */
  onClear: ($event: Event) => void;
  onButtonClick: ($event: Event) => void;
}
export interface IUseOption {
  name: ComputedRef<string | number | undefined>;
  onOptionClick: ($event: Event) => void;
  optionClass: ComputedRef<object>;
}

export interface IOption {
  value: string | number | undefined;
  name: string | undefined;
  disabled: boolean;
}

export type Options = Array<IOption>;

export interface ComboListContext {
  onValueChange: (item: IOption) => void;
  modelValue: ModelValue
}