import type { App } from 'vue';
import FComboList from './src/combo-list.component';

export * from './src/types';
export * from './src/combo-list.props';
export * from './src/composition';

export { FComboList };

export default {
  install(app: App): void {
    app.component(FComboList.name, FComboList);
  },
};
