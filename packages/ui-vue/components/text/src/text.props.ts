import { ExtractPropTypes } from 'vue';

export const textProps = {
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' }
};
export type TextProps = ExtractPropTypes<typeof textProps>;
