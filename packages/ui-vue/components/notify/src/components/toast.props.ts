import { ExtractPropTypes, PropType } from 'vue';
import { NotifyData, ToastyAnimate } from '../notify.props';

export const toastProps = {
    animate: { type: String as PropType<ToastyAnimate>, default: 'fadeIn' },
    options: { type: Object as PropType<NotifyData> }
};

export type ToastProps = ExtractPropTypes<typeof toastProps>;
