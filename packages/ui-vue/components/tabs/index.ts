import type { App } from 'vue';
import Tabs from './src/tabs.component';
import TabPage from './src/components/tab-page.component';

export * from './src/tabs.props';
export * from './src/components/tab-page.props';

export { Tabs, TabPage };

export default {
    install(app: App): void {
        app.component(Tabs.name, Tabs);
        app.component(TabPage.name, TabPage);
    }
};
