import { computed, defineComponent, provide, reactive, ref, SetupContext, toRef, onMounted, shallowRef, nextTick } from 'vue';
import { TabsProps, tabsProps } from './tabs.props';
import './tabs.css'
import { useTabs } from './composition/use-tabs';
import { TabContext, TabsContext } from './composition/types';
import { TabPageProps } from './components/tab-page.props';

export default defineComponent({
    name: 'FTabs',
    props: tabsProps,
    emits: [],
    setup(props: TabsProps, context: SetupContext) {
        // 标题Ul元素
        const TitleUlElement = shallowRef<HTMLElement>();

        const hideButtons = ref(true);

        const hideDropDown = ref(false);

        const hasInHeadClass = computed(() => {
            return false;
        });
        const menuItemsWidth = ref<string>('atuo')

        const searchTabText = ref<string>('');

        // 存储tab的props数组
        const tabs = ref<TabContext[]>([]);

        const activeId = ref(props.activeId);

        const { setActiveId, getTabStyle, getTabClass, getTabNavLinkClass, getTabTextClass, changeTitleStyle, selectTabByIndex } = useTabs(props, context, activeId);
        // 增加一个tab
        const addTab = (tabContext: TabContext) => {
            const index = tabs.value.findIndex(tab => tab.props.id === tabContext.props.id)
            if (index === -1) {
                tabs.value.push(tabContext)
            }
        };

        const updateTab = (tabContext: TabContext) => {
            const index = tabs.value.findIndex(tab => tab.props.id === tabContext.props.id)
            if (index === -1) {
                return
            }
            tabs.value.forEach(tab => {
                if (tab.props.id === tabContext.props.id) {
                    tab = tabContext
                }
            })
            nextTick(() => {
                changeTitleStyle(TitleUlElement)
            });
        }

        function removeTab($event: Event, targetTabId: string) {
            tabs.value = tabs.value.filter(tab => tab.props.id !== targetTabId)
            if (activeId.value === targetTabId) {
                activeId.value = ''
                setActiveId(tabs)
            }
            $event.preventDefault();
            $event.stopPropagation();
        }

        provide<TabsContext>('tabs', { activeId, addTab, updateTab, tabs });

        const shouldShowNavFill = computed(() => {
            return props.fill || props.tabType === 'fill'
        });

        const shouldShowNavPills = computed(() => {
            return props.tabType === 'pills'
        });

        const tabsHeaderClass = computed(() => ({
            'farris-tabs-header': true,
            'farris-tabs-inHead': hasInHeadClass.value,
            'farris-tabs-inContent': !hasInHeadClass.value,
            'farris-tabs-nav-fill': shouldShowNavFill.value,
            'farris-tabs-nav-pills': shouldShowNavPills.value
        }));

        const tabsTitleStyle = computed(() => ({
            width: hasInHeadClass.value ? (props.titleWidth ? `${props.titleWidth}%` : '') : ''
        }));

        const tabsTitleButtonClass = computed(() => ({
            btn: true,
            'sc-nav-btn': true,
            'px-1': true,
            'sc-nav-lr': true,
            'd-none': hideButtons.value
        }));

        const dropdownTitleContainerCls = computed(() => ({
            'btn-group': true,
            'sc-nav-btn': true,
            'dropdown': true,
            'd-none': hideButtons.value
        }));

        const dropdownMenuCls = computed(() => ({
            'dropdown-menu': true,
            'tabs-pt28': props.searchBoxVisible,
            'show': !hideButtons.value
        }));

        const rightArrowButtonClass = computed(() => ({
            btn: true,
            'sc-nav-rg': true,
            'd-none': hideButtons.value
        }));

        const getMenuItemsWidth = () => {
            return { width: menuItemsWidth.value };
        };

        const getDropdownItemCls = (tab: any) => {
            return {
                'dropdown-item': true,
                'text-truncate': true,
                'px-2': true,
                disabled: tab.disabled,
                active: tab.id = activeId.value,
                'd-none': tab.show !== true
            }
        };

        const stopBubble = ($event: MouseEvent) => {
            $event.preventDefault();
            $event.stopPropagation();
        };

        const searchTab = () => {

        };

        const _cpSelectTabByIndex = ($event: MouseEvent, id: string) => {
            selectTabByIndex($event, id);
        }

        const cpTabs = computed(() => {
            let _cpTabs: TabContext[] = []
            if (props.searchBoxVisible) {
                _cpTabs = tabs.value?.filter(tab => tab.props.title.includes(searchTabText.value))
            } else {
                _cpTabs = tabs.value?.slice()
            }
            return _cpTabs
        })

        const tabParentClass = computed(() => ({
            spacer: true,
            'f-utils-fill': true,
            'spacer-sides': !hideButtons.value && hideDropDown.value,
            'spacer-sides-dropdown': !hideButtons.value && !hideDropDown.value
        }));

        const tabContainerClass = computed(() => ({
            nav: true,
            'farris-nav-tabs': true,
            'flex-nowrap': true,
            'nav-fill': props.fill || props.tabType === 'fill',
            'nav-pills': props.tabType === 'pills',
            'flex-row': props.position === 'top' || props.position === 'bottom',
            'flex-column': props.position === 'left' || props.position === 'right'
        }));

        const tabsContainerClass = computed(() => ({
            'farris-tabs': true,
            'flex-column': props.position === 'top',
            'flex-column-reverse': props.position === 'bottom',
            'flex-row': props.position === 'left',
            'flex-row-reverse': props.position === 'right'
        }));

        onMounted(() => {
            nextTick(() => {
                changeTitleStyle(TitleUlElement)
            });
        })

        return () => {
            return (
                <>
                    <div class={tabsContainerClass.value}>
                        <div class={tabsHeaderClass.value}>
                            <div class="farris-tabs-title scroll-tabs" style={tabsTitleStyle.value}>
                                <button type="button" class={tabsTitleButtonClass.value}></button>
                                <div class={tabParentClass.value} style="width:100%">
                                    <ul class={tabContainerClass.value} ref={TitleUlElement}>
                                        {tabs.value.map((tabPage: any) => {
                                            const tab = tabPage.props
                                            const titleSlot = tabPage.slots.title
                                            return (
                                                <li class={getTabClass(tab)} style={getTabStyle(tab)}>
                                                    <a class={getTabNavLinkClass(tab)} onClick={($event) => selectTabByIndex($event, tab.id)}>
                                                        {titleSlot ? titleSlot() : <span class={getTabTextClass(tab)} title={tab.title}>{tab.title}</span>}
                                                        {tab.removeable && (
                                                            <span class="st-drop-close" onClick={($event) => removeTab($event, tab.id)}>
                                                                <i class="f-icon f-icon-close"></i>
                                                            </span>
                                                        )}
                                                    </a>
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                                <div class={dropdownTitleContainerCls.value}>
                                    <button type="button" class={rightArrowButtonClass.value}></button>
                                    <button class="btn dropdown-toggle-split dropdown-toggle"></button>
                                    <div class={dropdownMenuCls}>
                                        {props.searchBoxVisible && (<div onClick={($event) => stopBubble($event)} class="pb-1 tabs-li-absolute">
                                            <input type="text" class="form-control k-textbox" v-model={searchTabText.value} onKeyup={() => searchTab()} />
                                            <span class="f-icon f-icon-page-title-query tabs-icon-search"></span>
                                        </div>)}
                                        <ul class="tab-dropdown-menu--items" style={getMenuItemsWidth()}>
                                            {cpTabs.value.map(tab => {
                                                return (<li class={getDropdownItemCls} onClick={($event) => _cpSelectTabByIndex($event, tab.props.id)}>
                                                    <a class="dropdown-title">{tab.props.title}</a></li>)
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="farris-tabs-content">
                            {context.slots.default?.()}
                        </div>
                    </div >
                </>
            );
        };
    }
});
