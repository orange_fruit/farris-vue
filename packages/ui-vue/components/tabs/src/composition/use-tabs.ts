import { computed, ComputedRef, Ref, ref, SetupContext, ShallowRef } from 'vue';
import { TabsProps } from '../tabs.props';
import { TabPageProps } from '../components/tab-page.props';
import { TabContext } from './types';

export function useTabs(
    props: TabsProps,
    context: SetupContext,
    activeId: Ref<string | undefined>
): any {

    function setActiveId(tabs: Ref<TabContext[]>) {
        const index = tabs.value.findIndex(tab => tab.props.show !== false && !activeId.value && !tab.props.disabled)
        if (!activeId.value && index !== -1) {
            activeId.value = tabs.value[index].props.id
        }
    }
    function getTabStyle(tab: TabPageProps) {
        return { width: `${tab.tabWidth}px` };
    }
    function getTabClass(tab: TabPageProps) {
        return {
            'nav-item': true,
            'd-none': tab.show !== undefined ? !tab.show : false,
            'f-state-active': tab.id === activeId.value,
            'f-state-disable': tab.disabled,
        };
    }
    function getTabNavLinkClass(tab: TabPageProps) {
        return {
            'nav-link': true,
            'tabs-text-truncate': true,
            active: tab.id === activeId.value,
            disabled: tab.disabled,
        };
    }
    function getTabTextClass(tab: TabPageProps) {
        return {
            'st-tab-text': true,
            'farris-title-auto': props.autoTitleWidth
        };
    }
    function changeTitleStyle(TitleUlElement: ShallowRef<HTMLElement>) {
        if (props.autoTitleWidth) {
            return;
        }
        const $textEls = TitleUlElement.value?.querySelectorAll('.st-tab-text');
        if (!$textEls) {
            return
        }
        for (var k = 0; k < $textEls.length; k++) {
            let parentEl = $textEls[k].parentNode as any;
            if ($textEls[k].scrollWidth > parentEl['offsetWidth']) {
                if (!$textEls[k].classList.contains('farris-title-text-custom')) {
                    $textEls[k].classList.add('farris-title-text-custom')
                }
            } else {
                $textEls[k].classList.remove('farris-title-text-custom')
            }
        }
    }
    function selectTabByIndex($event: MouseEvent, targetTabId: string) {
        activeId.value = targetTabId
        $event.preventDefault();
        $event.stopPropagation();
    }

    return {
        setActiveId,
        getTabStyle,
        getTabClass,
        getTabNavLinkClass,
        getTabTextClass,
        changeTitleStyle,
        selectTabByIndex
    };
}
