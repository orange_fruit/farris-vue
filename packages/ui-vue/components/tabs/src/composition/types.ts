import { Ref, Slots } from "vue"
import { TabPageProps } from "../components/tab-page.props"

export interface TabContext {
    props: TabPageProps,
    slots: Slots
}
export interface TabsContext {
    activeId: Ref<string | undefined>,
    addTab(props: TabContext): void,
    updateTab(props: TabContext): void,
    tabs: Ref<TabContext[]>
}