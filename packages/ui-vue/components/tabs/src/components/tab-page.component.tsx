import { defineComponent, getCurrentInstance, inject, onMounted, SetupContext, watch, ref, onBeforeMount, onBeforeUpdate, onUnmounted } from 'vue';
import { TabContext, TabsContext } from '../composition/types';
import { TabPageProps, tabPageProps } from './tab-page.props';

export default defineComponent({
    name: 'FTabPage',
    props: tabPageProps,
    emits: [],
    setup(props: TabPageProps, context: SetupContext) {
        const tabs = inject<TabsContext>('tabs');
        const instance = getCurrentInstance()
        const showContent = ref(true);
        const tableContext: TabContext = {
            slots: context.slots,
            props
        }
        onMounted(() => {
            const index = tabs?.tabs.value.findIndex(tab => tab.props.id === props.id);
            if (!index || index === -1) {
                tabs?.addTab(tableContext);
            } else if (index > -1) {
                showContent.value = false
                console.warn(`已经存在id为${props.id}的页签啦`);
            }
        })
        onUnmounted(() => {

        })
        function getTabPageStyle() {
            return {
                display: props?.id === tabs?.activeId.value ? '' : 'none'
            }
        }
        watch(
            () => props,
            (value) => {
                tabs?.updateTab({
                    props: value,
                    slots: context.slots
                })
            },
            {
                immediate: true,
                deep: true
            }
        )
        return () => {
            const content = context.slots.default?.() as any
            return (showContent.value ? <div class="farris-tab-page" style={getTabPageStyle()}>{content}</div> : null)
        };
    }
});
