import { ExtractPropTypes, PropType } from 'vue';

export type TabType = 'fill' | 'pills' | 'default';
export type TabPosition = 'left' | 'right' | 'top' | 'bottom';

export const tabsProps = {
    tabType: { type: String as PropType<TabType>, default: 'default' },
    autoTitleWidth: { type: Boolean, default: false },
    titleLength: { type: Number, default: 7 },
    position: { type: String as PropType<TabPosition>, default: 'top' },
    showDropDwon: { type: Boolean, default: false },
    showTooltips: { type: Boolean, default: false },
    scrollStep: { type: Number, default: 1 },
    autoResize: { type: Boolean, default: false },
    closeable: { type: Boolean, default: false },
    selectedTab: { type: String, default: '' },
    width: { type: Number },
    height: { type: Number },
    searchBoxVisible: { type: Boolean, default: true },
    titleWidth: { type: Number, default: 0 },
    customClass: { type: String, default: '' },
    activeId: { type: String },
    fill: { type: Boolean, default: false }
};

export type TabsProps = ExtractPropTypes<typeof tabsProps>;
