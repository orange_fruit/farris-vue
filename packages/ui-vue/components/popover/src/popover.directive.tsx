import { App, ComputedRef } from "vue";

function showPopover($event: Event, binding: Record<string, any>, reference: HTMLElement) {
    $event.stopPropagation();
    const popoverInstance = binding.value as ComputedRef;
    popoverInstance.value.show(reference);
}

function hidePopover($event: Event, binding: Record<string, any>) {
    $event.stopPropagation();
    const popoverInstance = binding.value as ComputedRef;
    popoverInstance.value.hide();
}

const popoverDirective = {
    mounted: (element: HTMLElement, binding: Record<string, any>, vnode: any) => {
        let app: App | null;
        if (binding.modifiers.hover) {
            element.addEventListener('mouseenter', ($event: MouseEvent) => showPopover($event, binding, element));
            element.addEventListener('mouseleave', ($event: MouseEvent) => hidePopover($event, binding));
        } else if (binding.modifiers.click) {
            element.addEventListener('click', ($event: Event) => showPopover($event, binding, element));
        } else {
            element.addEventListener('mouseenter', ($event: MouseEvent) => showPopover($event, binding, element));
            element.addEventListener('mouseleave', ($event: MouseEvent) => hidePopover($event, binding));
        }
    },
    unMounted: (element: HTMLElement, binding: Record<string, any>, vnode: any) => {
    }
};
export default popoverDirective;
