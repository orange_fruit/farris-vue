import { computed, defineComponent, nextTick, ref, SetupContext, Teleport } from 'vue';
import { PopoverProps, popoverProps } from './popover.props';

import './popover.scss';

export default defineComponent({
    name: 'FPopover',
    props: popoverProps,
    emits: [],
    setup(props: PopoverProps, context: SetupContext) {
        let scrollLeft = 0;

        let scrollTop = 0;

        if (typeof window !== 'undefined') {
            // eslint-disable-next-line prefer-destructuring
            scrollLeft = document.documentElement.scrollLeft;
            // eslint-disable-next-line prefer-destructuring
            scrollTop = document.documentElement.scrollTop;
        }

        const arrowRef = ref<HTMLElement>();

        const popoverRef = ref<HTMLElement>();

        const position = ref(props.placement);

        const popoverLeftPosition = ref('0px');

        const popoverTopPosition = ref('0px');

        const shouldShowTitle = computed(() => !!props.title);

        const displayPopover = ref(false);

        const popoverClass = computed(() => {
            const originPopover = `popover in popover-${position.value}`;
            const bsPopover = `bs-popover-${position.value}`;
            const popoverClassObject = {} as any;
            popoverClassObject[originPopover] = true;
            popoverClassObject[bsPopover] = true;
            return popoverClassObject;
        });

        const popoverContainerClass = computed(() => ({
            'popover-content': true,
            'popover-body': true
        }));

        const popoverStyle = computed(() => {
            const styleObject = {
                left: popoverLeftPosition.value,
                top: popoverTopPosition.value
            };
            return styleObject;
        });

        async function show(reference: HTMLElement) {
            if (popoverRef.value && arrowRef.value) {
                displayPopover.value = true;
                await nextTick();
                const hostRect = reference.getBoundingClientRect();
                const arrowRect = arrowRef.value.getBoundingClientRect();
                const popoverRect = popoverRef.value?.getBoundingClientRect();
                popoverLeftPosition.value = `${hostRect.left + hostRect.width / 2 + scrollLeft}px`;
                popoverTopPosition.value = `${hostRect.top - (popoverRect.height + arrowRect.height) + scrollTop}px`;
            }
        }

        function hide() {
            displayPopover.value = false;
        }

        context.expose({ hide, show });

        return () => {
            return (
                <>
                    <Teleport to="body">
                        <div ref={popoverRef} class={popoverClass.value} style={popoverStyle.value} v-show={displayPopover.value}>
                            <div ref={arrowRef} class="popover-arrow arrow"></div>
                            {shouldShowTitle.value && <h3 class="popover-title popover-header">{props.title}</h3>}
                            <div class={popoverContainerClass.value}>{context.slots.default && context.slots?.default()}</div>
                        </div>
                    </Teleport>
                </>
            );
        };
    }
});
