import type { App } from 'vue';
import Popover from './src/popover.component';
import PopoverDirective from './src/popover.directive';

export * from './src/popover.props';

export { Popover, PopoverDirective };

export default {
    install(app: App): void {
        app.component(Popover.name, Popover);
        app.directive('popover', PopoverDirective);
    }
};
