import { createApp } from 'vue';
import './style.css';
import App from './app.vue';
import Tooltip from '../components/tooltip/index';
import Popover from '../components/popover/index';

const app = createApp(App);
app.use(Popover).use(Tooltip).mount('#app');
