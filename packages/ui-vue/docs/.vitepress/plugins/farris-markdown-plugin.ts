import MarkdownIt from 'markdown-it';
import mdContainer from 'markdown-it-container';
import { render } from '../utils/render';

export const farrisMarkdownPlugin = (md: MarkdownIt, options: any) => {
    md.use(mdContainer, 'demo', {
        validate(params: string) {
            return !!params.trim().match(/^demo\s*(.*)$/);
        },
        render(tokens: any[], index: number) {
            const isTagOpening = tokens[index].nesting === 1;
            const matchDescriptionResult = tokens[index].info.trim().match(/^demo\s*(.*)$/);
            const hasDescription = matchDescriptionResult && matchDescriptionResult.length > 1;
            if (isTagOpening) {
                const sourceCode = tokens[index + 1].type === 'fence' ? tokens[index + 1].content : '';
                return `<demo>${sourceCode ? `<!--vue-demo:${sourceCode}:vue-demo-->` : ''}`;
            } else {
                return '</demo>';
            }
        }
    });
    const lang = options?.lang || 'vue';
    const defaultRender = md.renderer.rules.fence;
    md.renderer.rules.fence = (tokens: any[], index: number, option: any, env: any, self: any) => {
        const token = tokens[index];
        const prevToken = tokens[index - 1];
        const isFenceInMDContainer = !!(prevToken && prevToken.nesting === 1 && prevToken.info.trim().match(/^demo\s*(.*)$/));
        const shouldOverrideRender = token.info.trim() === lang && isFenceInMDContainer;
        if (shouldOverrideRender) {
            const matchDescriptionResult = prevToken.info.trim().match(/^demo\s*(.*)$/);
            const hasDescription = matchDescriptionResult && matchDescriptionResult.length > 1;
            const description = hasDescription ? matchDescriptionResult[1] : '';
            return `
                ${description ?
                    `<template #description>
                        <div>${md.renderInline(description)}</div>
                    </template>`: ''
                }
                <template #highlight>
                    <div class="language-${lang}">${option.highlight(token.content, lang, '')}</div>
                </template>`
        }
        return defaultRender ? defaultRender(tokens, index, options, env, self) : '';
    };
    const originalRender = md.render;
    md.render = (src: string, env?: any) => {
        let result = originalRender.call(md, src, env);
        const startTag = '<!--vue-demo:'
        const endTag = ':vue-demo-->'
        const hasVueDemo = result.indexOf(startTag) > -1 && result.indexOf(endTag) > -1;
        if (hasVueDemo) {
            const { template, script, style } = render(result, options)
            result = template
            const data = md['__data'];
            const hoistedTags = data.hoistedTags || (data.hoistedTags = [])
            hoistedTags.push(script)
            hoistedTags.push(style)
        }
        return result
    }
};