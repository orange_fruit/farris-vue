const sidebar = [
    {
        text: '介绍',
        items: [{ text: '快速开始', link: '/guide/quick-start/' }]
    },
    {
        text: '通用',
        items: [
            { text: 'Button 按钮', link: '/components/button/' },
            { text: 'Icon 图标', link: '/components/icon/' }
        ]
    },
    {
        text: '导航',
        items: [{ text: 'Accordion 手风琴', link: '/components/accordion/' }]
    },
    {
        text: '布局',
        items: [
            {
                text: 'section 面板',
                link: '/components/section/'
            },
            { text: 'tabs 标签页', link: '/components/tabs/' }
        ]
    },
    {
        text: '录入数据',
        items: [
            { text: 'Button Edit 按钮输入框', link: '/components/button-edit/' },
            { text: 'Radio Group 单选组', link: '/components/radio-group/' },
            { text: 'Text 静态文本', link: '/components/text/' },
            { text: 'Combo List 选择输入框', link: '/components/combo-list/' },
            { text: 'Swtich 开关', link: '/components/switch/' }
        ]
    },
    {
        text: '展示数据',
        items: [
            { text: 'Avatar 头像', link: '/components/avatar/' },
            { text: 'tooltip 提示信息', link: '/components/tooltip/' }
        ]
    },
    {
        text: '反馈',
        items: [
            { text: 'notify 通知消息', link: '/components/notify/' },
            { text: 'popover 气泡提示', link: '/components/popover/' }
        ]
    }
];

export default sidebar;
