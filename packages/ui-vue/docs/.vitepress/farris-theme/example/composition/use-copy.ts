import { computed, ComputedRef, ref, Ref, SetupContext } from "vue";
import { UseCopy } from "./type";
import message from '../messages/message';
import clipboardCopy from "../utils/clipboard-copy";

export function useCopy(props: any, context: SetupContext, locale: ComputedRef<any>): UseCopy {

    const isShowTip = ref(false);

    const copyText = computed(() => {
        return isShowTip.value ? locale.value['copy-success-text'] : locale.value['copy-button-text'];
    });

    async function onCopy() {
        try {
            await clipboardCopy(props.sourceCode);
            message.info(locale.value['copy-success-text']);
        } catch (err) {
            message.error(locale.value['copy-success-text']);
        }
    }

    return { copyText, onCopy };
}
