# Tabs 标签页

Tabs 组件为开发者提供具有页签导航的布局容器。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-tabs>
        <f-tab-page id="tab1" title="Tab Page 1">Content of Tab Page 1</f-tab-page>
        <f-tab-page id="tab2" title="Tab Page 2">Content of Tab Page 2</f-tab-page>
        <f-tab-page id="tab3" title="Tab Page 3">Content of Tab Page 3</f-tab-page>
    </f-tabs>
</template>
```

:::

## 禁用

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-tabs>
        <f-tab-page id="tab1" title="Tab Page 1">Content of Tab Page 1.</f-tab-page>
        <f-tab-page id="tab2" title="Disabled Tab Page" :disabled="true">Content of Tab Page Disabled.</f-tab-page>
        <f-tab-page id="tab3" title="Tab Page 2">Content of Tab Page 2.</f-tab-page>
    </f-tabs>
</template>
```

:::

## 隐藏

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-tabs>
        <f-tab-page id="tab1" title="Tab Page 1" :show="false">Content of Hidden Tab Page. </f-tab-page>
        <f-tab-page id="tab2" title="Tab Page 2">Content of Tab Page 2. Tab Page 1 is hidden, see example code to get more.</f-tab-page>
        <f-tab-page id="tab3" title="Tab Page 3">Content of Tab Page 3.</f-tab-page>
    </f-tabs>
</template>
```

:::

## 关闭按钮

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-tabs>
        <f-tab-page id="tab1" title="Tab Page 1" :removeable="true">Content of Tab Page 1.</f-tab-page>
        <f-tab-page id="tab2" title="Tab Page 2" :removeable="true">Content of Tab Page 2.</f-tab-page>
        <f-tab-page id="tab3" title="Tab Page 3" :removeable="true">Content of Tab Page 3.</f-tab-page>
    </f-tabs>
</template>
```

:::

## 类型

```typescript
export type TabType = 'fill' | 'pills' | 'default';
export type TabPosition = 'left' | 'right' | 'top' | 'bottom';
```

## 属性

| 属性名           | 类型                    | 默认值    | 说明                     |
| :--------------- | :---------------------- | :-------- | :----------------------- |
| tabType          | `string as TabType`     | 'default' | 标签页显示样式           |
| autoTitleWidth   | `boolean`               | false     | 是否自动调整标题宽度     |
| titleLength      | `number`                | 7         | 标题宽度                 |
| position         | `string as TabPosition` | 'top'     | 显示页签的位置           |
| showDropDwon     | `boolean`               | false     | 是否显示页签导航下拉按钮 |
| showTooltips     | `boolean`               | false     | 是否显示标题提示信息     |
| scrollStep       | `number`                | 1         | 滚动鼠标切换页签的步长   |
| autoResize       | `boolean`               | false     | 是否允许自动调整页签高度 |
| selectedTab      | `string`                | --        | 指定选中的页签           |
| width            | `number`                | --        | 组件宽度                 |
| height           | `number`                | --        | 组件高度                 |
| searchBoxVisible | `boolean`               | false     | 是否显示页签搜索框       |
| titleWidth       | `number`                | --        | 标题宽度                 |
| customClass      | `string`                | --        | 标签自定义样式           |
| activeId         | `string`                | --        | 被激活的页签标识         |

## 插槽

::: tip
暂无内容
:::
