# Text 静态文本

Text 组件用来展示只读态的页面数据。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('一段用于展示的静态文本');
</script>
<template>
    <f-text v-model="text"> </f-text>
</template>
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
