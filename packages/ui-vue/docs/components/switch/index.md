# Switch 开关

Switch 组件用来切换数据状态。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const status = ref(true);
</script>
<template>
    <f-switch v-model="status"></f-switch>
</template>
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
