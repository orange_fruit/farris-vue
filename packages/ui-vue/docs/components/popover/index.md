# Popover 气泡提示

Tooltip 组件用来实现鼠标移动至目标组件时，或者点击目标时，在其上显示提示丰富的信息。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { computed, ref } from 'vue';
const popoverRef = ref<HTMLElement>();
const popoverInstance = computed(() => popoverRef);
</script>
<template>
    <f-popover ref="popoverRef" :title="'This the title'"> This is popover content. </f-popover>
    <f-button v-popover="popoverInstance"> hover to show popover </f-button>
</template>
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
