# Button Edit 按钮输入框

Button Edit 是允许在其编辑器内部嵌入按钮的文本输入框。开发者可以自定义输入框的按钮，以便于实现丰富的录入交互形式。

## 基本用法

:::demo 使用`v-model`对输入值做双向绑定。

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('');
</script>
<template>
    <div class="my-2">
        <span>输入框文本: </span>
        <span>{{ text }}</span>
    </div>
    <f-button-edit v-model="text"></f-button-edit>
</template>
```

:::

## 对齐方式

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('Button Edit 按钮输入框');
const alignLeft = ref('left');
const alignCenter = ref('center');
const alignRight = ref('right');
</script>
<template>
    <div class="my-2">
        <span>缺省对齐: </span>
    </div>
    <f-button-edit v-model="text"></f-button-edit>
    <div class="my-2">
        <span>居左对齐: </span>
    </div>
    <f-button-edit :text-align="alignLeft" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>居中对齐: </span>
    </div>
    <f-button-edit :text-align="alignCenter" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>居右对齐: </span>
    </div>
    <f-button-edit :text-align="alignRight" v-model="text"></f-button-edit>
</template>
```

:::

## 状态

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('Button Edit 按钮输入框');
const editable = ref(false);
const disable = ref(true);
const readonly = ref(true);
</script>
<template>
    <div class="my-2">
        <span>默认状态: </span>
    </div>
    <f-button-edit v-model="text"></f-button-edit>
    <div class="my-2">
        <span>只读状态: </span>
    </div>
    <f-button-edit :readonly="readonly" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>不可编辑状态: </span>
    </div>
    <f-button-edit :editable="editable" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>禁用状态: </span>
    </div>
    <f-button-edit :disable="disable" v-model="text"></f-button-edit>
</template>
```

:::

## 按钮

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('');
const textButtonContent = ref('查询');
const searchIconContent = ref('<i class="f-icon f-icon-search"></i>');
const passwordIconContent = ref('<i class="f-icon f-icon-eye"></i>');
</script>
<template>
    <div class="my-2">
        <span>缺省按钮: </span>
    </div>
    <f-button-edit v-model="text"></f-button-edit>
    <div class="my-2">
        <span>文本按钮: </span>
    </div>
    <f-button-edit :button-content="textButtonContent" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>查询按钮: </span>
    </div>
    <f-button-edit :button-content="searchIconContent" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>密码按钮: </span>
    </div>
    <f-button-edit :button-content="passwordIconContent" v-model="text"></f-button-edit>
</template>
```

:::

## 清空按钮

:::demo 使用清空按钮可以快速置空文本框。

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('Button Edit 按钮输入框');
const enableClear = ref(true);
</script>
<template>
    <div class="my-2 d-flex">
        <div class="f-demo-label">显示清空按钮:</div>
        <f-switch v-model="enableClear"></f-switch>
    </div>
    <f-button-edit :enable-clear="enableClear" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>只读状态: </span>
    </div>
    <f-button-edit :enable-clear="enableClear" readonly v-model="text"></f-button-edit>
    <div class="my-2">
        <span>禁用状态: </span>
    </div>
    <f-button-edit :enable-clear="enableClear" disable v-model="text"></f-button-edit>
</template>
<style scoped>
.f-demo-label {
    line-height: 1.4rem;
    margin-right: 8px;
}
</style>
```

:::

## 文本标签

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('Button Edit 按钮输入框');
const enableTitle = ref(true);
</script>
<template>
    <div class="my-2">
        <span>鼠标悬停在输入框上方，查看文本标签: </span>
    </div>
    <f-button-edit :enable-title="enableTitle" v-model="text"></f-button-edit>
</template>
```

:::

## 提示信息

:::demo 使用清空按钮可以快速置空文本框。

```vue
<script setup lang="ts">
import { ref } from 'vue';
const text = ref('');
const buttonEditPlaceholder = ref('请输入...');
const forcePlaceholder = ref(true);
</script>
<template>
    <div class="my-2">
        <span>缺省情况: </span>
    </div>
    <f-button-edit :placeholder="buttonEditPlaceholder" v-model="text"></f-button-edit>
    <div class="my-2">
        <span>在缺省情况下，只读状态不显示提示信息: </span>
    </div>
    <f-button-edit :placeholder="buttonEditPlaceholder" readonly v-model="text"></f-button-edit>
    <div class="my-2">
        <span>在缺省情况下，禁用状态不显示提示信息: </span>
    </div>
    <f-button-edit :placeholder="buttonEditPlaceholder" disable v-model="text"></f-button-edit>
    <div class="my-2">
        <span>在只读状态下显示提示信息: </span>
    </div>
    <f-button-edit :placeholder="buttonEditPlaceholder" :force-placeholder="forcePlaceholder" readonly v-model="text"></f-button-edit>
    <div class="my-2">
        <span>在禁用状态下显示提示信息: </span>
    </div>
    <f-button-edit :placeholder="buttonEditPlaceholder" :force-placeholder="forcePlaceholder" disable v-model="text"></f-button-edit>
</template>
```

:::

## 类型

```typescript
type TextAlignment = 'left' | 'center' | 'right';
```

## 属性

| 属性名                 | 类型                      | 默认值 | 说明                                                                          |
| :--------------------- | :------------------------ | :----- | :---------------------------------------------------------------------------- |
| id                     | `string`                  | --     | 标记组件的唯一标识                                                            |
| autoComplete           | `boolean`                 | false  | 是否启用自动完成录入文本功能                                                  |
| buttonContent          | `string`                  | --     | 按钮内容，缺省为点击按钮图标                                                  |
| customClass            | `string`                  | --     | 组件自定义样式                                                                |
| disable                | `boolean`                 | false  | 将组件设置为禁用状态                                                          |
| editable               | `boolean`                 | true   | 是否允许在输入框编辑文本                                                      |
| enableClear            | `boolean`                 | false  | 是否启用情况按钮                                                              |
| readonly               | `boolean`                 | false  | 将组件输入框设置为只读                                                        |
| textAlign              | `string as TextAlignment` | 'left' | 设置输入框对齐方式，缺省状况为左对齐，可选择的值包括：'left','center','right' |
| showButtonWhenDisabled | `boolean`                 | false  | 是否允许在禁用组件时仍然显示组件按钮                                          |
| enableTitle            | `boolean`                 | false  | 是否启用输入框的的提示文本                                                    |
| inputType              | `'text' \| 'password'`    | 'text' | 设置输入框类型，缺省为文本输入框，可以选择设为密码输入框                      |
| forcePlaceholder       | `boolean`                 | false  | 是否始终显示输入框提示文本                                                    |
| placeholder            | `string`                  | --     | 设置输入框提示文本                                                            |
| minLength              | `number`                  | --     | 设置输入框文本最小长度                                                        |
| maxLength              | `number`                  | --     | 设置输入框文本最大长度                                                        |
| tabIndex               | `number`                  | --     | 设置组件 Tab 键索引                                                           |

## 事件

| 事件名         | 类型                | 说明                 |
| :------------- | :------------------ | :------------------- |
| clear          | `EventEmitter<any>` | 点击清空按钮事件     |
| change         | `EventEmitter<any>` | 输入框值变化事件     |
| click          | `EventEmitter<any>` | 点击输入框事件       |
| clickButton    | `EventEmitter<any>` | 点击按钮事件         |
| blur           | `EventEmitter<any>` | 输入框失去焦点事件   |
| focus          | `EventEmitter<any>` | 输入框获得焦点事件   |
| mouseEnterIcon | `EventEmitter<any>` | 鼠标移入按钮事件     |
| mouseLeaveIcon | `EventEmitter<any>` | 数据移出按钮事件     |
| keyup          | `EventEmitter<any>` | 抬起键盘按键事件     |
| keydown        | `EventEmitter<any>` | 按下键盘按键事件     |
| input          | `EventEmitter<any>` | 在输入框录入字符事件 |

## 插槽

::: tip
暂无内容
:::
