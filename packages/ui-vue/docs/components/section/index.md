# Section 面板

Section 组件为开发者提供了一个具有标题的组件容器。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-section main-title="This is section title">
        <div>This is section component.</div>
    </f-section>
</template>
```

:::

## 类型

```typescript
export interface ButtonAppearance {
    class: string;
}

export interface ButtonConfig {
    id: string;
    disable: boolean;
    title: string;
    click: any;
    appearance: ButtonAppearance;
    visible?: boolean;
}

export interface ToolbarConfig {
    position: string;
    contents: ButtonConfig[];
}
```

## 属性

| 属性名                | 类型                      | 默认值 | 说明                           |
| :-------------------- | :------------------------ | :----- | :----------------------------- |
| customClass           | `string`                  | --     | 组件自定义样式                 |
| maxStatus             | `boolean`                 | false  | 组件最大化状态                 |
| enableCollapse        | `boolean`                 | true   | 是否允许收折                   |
| mainTitle             | `string`                  | --     | 面板标题                       |
| subTitle              | `string`                  | --     | 面板副标题                     |
| showHeader            | `boolean`                 | true   | 是否显示标题面板               |
| enableMaximize        | `boolean`                 | false  | 是否显示最大化按钮             |
| fill                  | `boolean`                 | false  | 是否允许填充显示               |
| expandStatus          | `boolean`                 | true   | 组件展开状态                   |
| context               | `object`                  | --     | 组件上下文                     |
| index                 | `number`                  | --     | 索引号                         |
| toolbarPosition       | `string`                  | --     | 工具栏位置                     |
| toolbarButtons        | `object[]`                | []     | 工具栏按钮                     |
| toolbar               | `object as ToolbarConfig` | {}     | 工具栏配置对象                 |
| showToolbarMoreButton | `boolean`                 | true   | 是否显示工具栏的更多按钮       |
| clickThrottleTime     | `number`                  | 350    | 鼠标重复点击按钮触发事件的阈值 |
| headerClass           | `string`                  | ''     | 标题面板自定义样式             |

## 插槽

::: tip
暂无内容
:::
