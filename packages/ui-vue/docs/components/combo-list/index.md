# Combo List 选择输入框

Combo List 组件提供下拉框形式选择枚举数据。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-combo-list></f-combo-list>
</template>
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
